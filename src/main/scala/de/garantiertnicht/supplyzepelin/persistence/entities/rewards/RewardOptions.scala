/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.persistence.entities.rewards

import scala.language.implicitConversions

case class RewardOptions(optPriority: Option[Int], optRemove: Option[Int]) {
  def priority: Int = optPriority.getOrElse(0)
  def remove: Boolean = optRemove.getOrElse(0) == 0
  def persist: Boolean = !optRemove.contains(-1)

  override def toString = {
    val stringPriority = if(optPriority.nonEmpty) s"PRIOTITY $priority" else null
    val stringRemove = optRemove match {
      case None ⇒ null
      case Some(0) ⇒ "LOSABLE"
      case Some(1) ⇒ "PERMANENT"
      case Some(-1) ⇒ "SINGLE"
    }

    Seq(stringPriority, stringRemove).filter(_ != null).mkString(", ")
  }

  def changeTo(rewardOptions: RewardOptions): Option[RewardOptions] = {
    val changed = RewardOptions(
      rewardOptions.optPriority.orElse(optPriority),
      rewardOptions.optRemove.orElse(optRemove)
    )

    if(changed == RewardOptions.default) {
      None
    } else {
      Some(changed)
    }
  }
}

object RewardOptions {
  val default = RewardOptions(None, None)
  implicit def option2default(option: Option[RewardOptions]): RewardOptions = option.getOrElse(default)
}
