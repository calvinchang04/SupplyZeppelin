/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot

import akka.actor.{Actor, PoisonPill, ReceiveTimeout}
import akka.event.Logging
import akka.util.Timeout
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.bot.command.CommandExecution
import de.garantiertnicht.supplyzepelin.bot.deployment.NewBribeCalculations
import de.garantiertnicht.supplyzepelin.persistence.definitions._
import de.garantiertnicht.supplyzepelin.persistence.entities._
import de.garantiertnicht.supplyzepelin.persistence.entities.rewards.{Collect, Event, PointTransfer}
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.{Guild, Member, Message, TextChannel}
import org.mongodb.scala.Observer
import org.mongodb.scala.result.UpdateResult

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.language.postfixOps

case class Competition(message: Message, points: Long) {
  def isForCompetition(reaction: GuildReaction): Boolean = {
    reaction.reaction.getGuild.getSelfMember.hasPermission(reaction.reaction.getTextChannel, Permission.MESSAGE_HISTORY, Permission.MESSAGE_READ, Permission.MESSAGE_WRITE, Permission.MESSAGE_ADD_REACTION) && reaction.reaction.getMessageIdLong == this.message.getIdLong && reaction.reaction.getChannel.getIdLong == this.message.getChannel.getIdLong
  }
}

object GuildActorShouldBePoisoned
case class KillCompetition(competition: Competition)
case class PermissionCheck(time: Byte)
case class Bribe(member: Member, amount: Long)
case class Block(member: UserId)
case class Unblock(member: UserId)
case class GuildInfo(channel: TextChannel)
case class GetBlockedStatus(member: Member)

case class Transaction(amount: Points, to: Seq[Member], source: Option[Event])

case class GuildBotMessage(message: Message)

class GuildActor(guild: Guild) extends Actor {
  /** The current activity for that guild */
  var activityCount = 0
  /** The last sender. Activity will only be granted for messages that have a different sender. */
  var lastSender: Long = _
  /** Time the last competition ended as Linux Epoch */
  var lastTime: Long = System.currentTimeMillis() / 1000
  /** The currently active competition (in the state that clicking the reaction will grant the reward) */
  var activeCompetition: Competition = _

  /** The competition that is in the state of users receiving penalties if they click the reaction. */
  var lastCompetition: Competition = activeCompetition
  /** Users that were already recieved a penalty for clicking a reaction of a semi-closed contest. */
  val leftOverReceivedUsers: ListBuffer[Member] = new ListBuffer[Member]
  /** Standard message to signify a crate has been dropped. */
  val lastCompetitionAwardMessageStandard = "The crate has been collected!"
  /**
    * The message that was created for the last competition. Will be null if there is no message that is
    * in the "click for penalty" state.
    */
  var lastCompetitionAwardMessage: String = lastCompetitionAwardMessageStandard

  /** This is the activity that is expected to be made within 10 minutes. */
  var standardActivity: Int = Math.max(guild.getMembers.size() / 6, 141)

  /** Counts the remaining drops for this guild until the score will be negative once. */
  var negativeCounter: Int = (math.random * 7).toInt

  /** A list of all bribes collected */
  var bribes = new ListBuffer[Bribe]
  var bribesForCompetition = new ListBuffer[Bribe];

  /** Timeout for asks */
  implicit val timeout = Timeout(1 minute)
  /** The dispatcher where ask-s will be executed. */
  implicit val dispatcher: ExecutionContextExecutor = context.system.dispatcher

  /** The logger to log to */
  val log = Logging(context.system, this)

  /** If the guild should stop (mostly because ;stop) */
  var shouldStop = false

  val blockedMembers = new mutable.ListBuffer[Long]()

  val PATTERN_REDBOT_TRANSFER_MESSAGE = "^([1-9][0-9]*) credits have been transferred to Supply Zeppelin's account\\.$".r
  val PATTERN_REDBOT_TRANSFER_COMMAND = s"^(.*)bank +transfer +<@!?${guild.getJDA.getSelfUser.getId}> +([0-9]+).*".r

  context.setReceiveTimeout(15 minutes)

  override def receive: Receive = {
    case message: GuildMessage =>
      // TODO may still be too long
      val now = System.currentTimeMillis() / 1000
      val diff = now - lastTime

      /* We increase the activity only if all of the below is true:
          * We can read, write, add our external emoji, read message history (to edit) and remove emotes (Manage Messages)
          * Either we should stop (;stop), or the last contest is more then 90s ago and the last sender is not the sender of this message.
      */
      if(message.message.getGuild.getSelfMember.hasPermission(message.message.getTextChannel, Permission.MESSAGE_READ, Permission.MESSAGE_EXT_EMOJI, Permission.MESSAGE_WRITE, Permission.MESSAGE_HISTORY, Permission.MESSAGE_ADD_REACTION, Permission.MESSAGE_MANAGE) && !isBlocked(message.message.getMember) && (shouldStop || (diff > 90 && message.message.getAuthor.getIdLong != lastSender))) {
        lastSender = message.message.getAuthor.getIdLong
        // The activity is based on the character count: n < 5 = 0; n >= 5 && n < 140 = n; after that its linearly decreasing towards 0 at 2000.
        // Also, we will double/quadruple/… if the contest is running for 20/40/60/… minutes to make drops more likely
        activityCount += activity(message.message.getRawContent.length) * (1 << (diff / 1200))

        if(activityCount > standardActivity || shouldStop) {
          if(activeCompetition == null) {
            // If the activity reaches the guessed activity for 10 minutes AND there is no active competition, we drop a crate.
            startCompetition(message.message.getTextChannel, diff, standardActivity)
          }

          // Reset stats.
          activityCount = 0
          lastTime = System.currentTimeMillis() / 1000
          standardActivity = math.max(((standardActivity * 600 / (diff * (diff / 1200 + 1)) + standardActivity * 2) / 3).toInt, 141)
        }
      }
    case competition: Competition =>
      // Really open the competition.
      activeCompetition = competition
      competition.message.addReaction(Main.reaction).queue()
      // Time out after one minute.
      context.system.scheduler.scheduleOnce(1 minute, self, KillCompetition(competition))
    case kill: KillCompetition => killCompetition(kill) // Forcefully ends the competition
    case GuildActorShouldBePoisoned =>
      // We unload the actor if we received no message on it after 15 minutes.
      // Can also occur on ;stop.
      if(activeCompetition != null) {
        // Kill the competition in the case of one being active
        self ! KillCompetition(activeCompetition)
      }

      // Only unload if there are bribes active.
      if(bribes.isEmpty && activeCompetition == null) {
        context become stopping
        self ! PoisonPill
      } else {
        shouldStop = true
        context.system.scheduler.scheduleOnce(5 minutes, self, PoisonPill)
      }
    case _: ReceiveTimeout =>
      // This is called once the guild is inactive for 15 minutes.
      if(bribes.isEmpty) {
        // We don't naturely unload if there are pending bribes as they are not persistent.
        self ! GuildActorShouldBePoisoned
      }
    case reaction: GuildReaction =>
      // Received if a reaction is added to a guild.
      if(!isBlocked(reaction.author)) {
        closeCompetition(reaction)
      }
    case command: GuildCommand =>
      // This happens if the message starts with a `;` and the sender is not a bot.
      if(!isBlocked(command.message.getMember) && !shouldStop) {
        Main.bot ! CommandExecution(command, self)
      }
    case bribe: Bribe =>
      // Add a bribe.
      bribes += bribe
    case Block(member) =>
      // Add a user to the blocked list.
      if(!blockedMembers.contains(member)) {
        blockedMembers += member
      }
    case Unblock(member) =>
      // Remove a user from the blocked list.
      if(blockedMembers.contains(member)) {
        blockedMembers -= member
      }
    case GuildBotMessage(message) ⇒
      if(message.getGuild.getSelfMember.hasPermission(message.getTextChannel, Permission.MESSAGE_READ, Permission.MESSAGE_WRITE, Permission.MESSAGE_HISTORY)) {
        interopabilityAcceptRedBotPoints(message)
      }
    case GetBlockedStatus(member) ⇒ sender ! isBlocked(member)
    case GuildInfo(channel) =>
      // Send the guild info in a channel.
      channel.sendMessage(
        s"activityCount=$activityCount\n" +
        s"standartActivity=$standardActivity\n" +
        s"timeoutDiff=${System.currentTimeMillis() / 1000 - lastTime}\n" +
        s"pendingBribeAmount=${bribesForCompetition.length + bribes.length}\n" +
        s"lastCompetition=$lastCompetition\n" +
        s"activeCompetition=$activeCompetition"
      ).queue()
    case Transaction(points, to, source) ⇒ modify(points, source, to: _*)
  }

  def modify(amount: Points, source: Option[Event], to: Member*): Unit = {
    val actualAmount = amount.min(Main.MAX_POINT_MODIFICATION).max(-Main.MAX_POINT_MODIFICATION)

    to.filter(member => !member.getUser.isBot && !isBlocked(member)).foreach { member ⇒
      new GetInfoForMember(member).mongoQuery.head().onSuccess{
        case null ⇒
          UpdateScore(member, Score(actualAmount)).mongoQuery.subscribe(new Observer[UpdateResult] {
            override def onError(e: Throwable) = e.printStackTrace()
            override def onComplete() = {}
            override def onNext(result: UpdateResult) = {}
          })
        case entry: ScoreboardEntry ⇒
          val score = entry.score.total
          val transfer = if(score >= 0) {
            val maxAmount = Main.MAX_POINTS - score
            math.min(actualAmount, maxAmount)
          } else {
            val maxAmount = Main.MAX_POINTS - score.abs
            math.max(actualAmount, -maxAmount)
          }

          UpdateScore(member, Score(transfer)).mongoQuery.subscribe(new Observer[UpdateResult] {
            override def onError(e: Throwable) = e.printStackTrace()
            override def onComplete() = {}
            override def onNext(result: UpdateResult) = {}
          })
        }
    }

    source.foreach(event => context.system.scheduler
      .scheduleOnce(((1000 + to.size.max(50) * 50) * 20) milliseconds, Main.bot, Recalculate(guild, event, to)))
  }

  def startCompetition(channel: TextChannel, diff: Long, standardActivity: Int): Unit = {
    // Change the bribes to the new competition so no new ones can be added for this
    bribesForCompetition ++= bribes
    bribes.clear()

    // Calculate the points
    var points = Math.min(getPointsForCompetition(diff, standardActivity), Main.MAX_POINT_MODIFICATION - 1)

    // Swap sing to create negative crates now and then
    if(negativeCounter <= 0) {
      points = -points
      negativeCounter = (math.random * 10 + 10).toInt

      // Check if the guild has a weak security policy regularly as some big guilds may never reload.
      if(hasElevatedPermissionsInGuild(guild)) {
        disableGuildAndNotifyOwnerSecurity(guild)
      }
    }

    negativeCounter -= 1

    // Check if bribes are present and tell that information
    val bribesPresent = if(bribesForCompetition.nonEmpty) {
      "+ bribes"
    } else {
      "with no additional `;bribe`s"
    }

    // Send message
    channel
      .sendMessage(s"A supply crate with $points points $bribesPresent has dropped! Click the reaction to open it! " +
        s"**Note**: By clicking the reaction you agree to the Terms of Service of garantiertnicht bots, located at https://discord.gg\\fX5kBdS")
      .queue(message => self ! Competition(message, points))

    killCompetition(KillCompetition(lastCompetition))
  }

  /**
    * Returns how many points a crate should drop.
    * @param diff The difference in secounds sience the last crate dropped.
    * @return
    */
  def getPointsForCompetition(diff: Long, standardActivity: Int): Long = {
    Math.max((1 - (diff / 1200.0)) * 100 * (standardActivity / 1000.0), 10 * (standardActivity / 1000.0)).toLong
  }

  def getBribeAmount(droppedAmount: Long, bribedAmount: Long, owner: Boolean): Long = {
    val isNegativeDrop = droppedAmount < 0
    val isNegativeBribe = bribedAmount < 0
    val absoluteBribeValue = bribedAmount.abs

    val maximumBonus = if(isNegativeDrop == isNegativeBribe) {
      3.75
    } else {
      1.75
    }

    val bonus = (10000 / (10000 + absoluteBribeValue).toDouble) * maximumBonus
    val absoluteAddedValue = (bribedAmount * bonus).toLong
    val newMaximumValue = absoluteBribeValue + absoluteAddedValue

    val newValue = if(owner) {
      newMaximumValue
    } else {
      newMaximumValue / 4
    }

    if(isNegativeBribe) {
      -newValue
    } else {
      newValue
    }
  }

  // TODO Split this up, it is a bit long
  def closeCompetition(reaction: GuildReaction): Unit = {
    if(reaction.reaction.getEmote.getEmote == Main.reaction) {
      if (activeCompetition != null && activeCompetition.isForCompetition(reaction)) {
        var collectedPoints = activeCompetition.points
        var bribesInvolved = if(bribesForCompetition.nonEmpty) {
          " with bribes being involved (o.0)"
        } else {
          ""
        }

        bribesForCompetition.foreach(bribe => {
          if(collectedPoints < Main.MAX_POINT_MODIFICATION) {
            if(NewBribeCalculations.isEligible(guild)) {
              collectedPoints += getBribeAmount(activeCompetition.points, bribe.amount, reaction.author == bribe.member)
            } else {
              if(bribe.member == reaction.author) {
                collectedPoints += bribe.amount * 2
              } else {
                collectedPoints += bribe.amount / 3
              }
            }
          } else {
            collectedPoints = Main.MAX_POINT_MODIFICATION - 1
          }
        })

        bribesForCompetition.clear()
        collectedPoints = collectedPoints.min(Main.MAX_POINT_MODIFICATION - 1).max(-Main.MAX_POINT_MODIFICATION + 1)

        modify(collectedPoints, Some(Collect()), reaction.author)

        lastCompetitionAwardMessage = s"${reaction.author.getAsMention} got $collectedPoints points from a supply crate$bribesInvolved!"

        lastCompetition = activeCompetition.copy(points = collectedPoints)
        activeCompetition = null

        if(shouldStop) {
          self ! KillCompetition(activeCompetition)
          return
        } else {
          lastCompetition.message.editMessage(lastCompetitionAwardMessage + s" No worries, you still can get ${getLeftoversForCompetition(lastCompetition)} points by clicking the reaction" +
            " **Note**: By clicking the reaction you agree to the Terms of Service of garantiertnicht bots, located at <https://discord.gg\\fX5kBdS>!").queue()
        }

        activityCount += 140
        lastTime = System.currentTimeMillis() / 1000

        leftOverReceivedUsers += reaction.author

        if(!reaction.reaction.getGuild.getPublicRole.hasPermission(reaction.reaction.getTextChannel, Permission.MESSAGE_READ) && reaction.reaction.getGuild.getPublicChannel.canTalk) {
          reaction.reaction.getGuild.getPublicChannel.sendMessage(s"${Main.reaction.getAsMention} ${reaction.author.getAsMention} has collected $collectedPoints in a private channel.").queue()
        }
      } else if(lastCompetition != null && lastCompetition.isForCompetition(reaction)) {
        if(!leftOverReceivedUsers.contains(reaction.author)) {
          val leftovers = getLeftoversForCompetition(lastCompetition)
          modify(leftovers, Some(Collect()), reaction.author)
        }

        leftOverReceivedUsers += reaction.author
      }
    }
  }

  /**
    * This state means that no commands will be processed.
    * We can only close competitions (but not start them) or check if we still have elevated permissions.
    */
  def stopping: Receive = {
    case PermissionCheck(time) =>
      if(!hasElevatedPermissionsInGuild(guild)) {
        context.unbecome()
      } else {
        if(time == 5) {
          self ! PoisonPill
        } else {
          context.system.scheduler.scheduleOnce(10 minutes, self, PermissionCheck((time + 1).toByte))
        }
      }
    case _ => // ignored
  }

  override def preStart(): Unit = {
    if(hasElevatedPermissionsInGuild(guild)) {
      // We don't want to be on guilds that have a "questionable security policy".
      disableGuildAndNotifyOwnerSecurity(guild)
    }

    // This may result in blocked users being able to do one command
    new GetBlockedMembersForGuild(guild).mongoQuery.toFuture().onSuccess {
      case list: Seq[ScoreboardEntry] => list.foreach(entry ⇒ self ! Block(entry._id.userId))
    }
  }

  def disableGuildAndNotifyOwnerSecurity(guild: Guild): Unit = {
    guild.getOwner.getUser.openPrivateChannel().queue(channel => {
      channel.sendMessage(s"I have administrator permissions on your server ${guild.getName} which is bad. It could allow me to go " +
        "rouge on your server or allow persons who find out the Bot token to do the same. Due to security reasons stated above, the bot " +
        "will not work until you fix these issues (it will check every 10 minutes)." +
        "\n" +
        "TL;DR: Don't give me a role which has administrator permissions, I do not need them.").queue()
    })

    context become stopping
    context.system.scheduler.scheduleOnce(10 minutes, self, PermissionCheck(0))

    log.info(s"Unsafe guild ${guild.getId} ${guild.getName}")
  }

  /**
    * This method checks if the bot has access to manage roles. This will
    * @param guild The guild to check
    * @return If the bot has elevated permissions in this guild.
    */
  def hasElevatedPermissionsInGuild(guild: Guild): Boolean = {
    guild.getSelfMember.hasPermission(Permission.ADMINISTRATOR)
  }

  override def postStop(): Unit = {
    // Unregister ourselfs
    Main.bot ! GuildActorPoisoned(guild)
    super.postStop()
  }

  /**
    * Get the activity for a given character length
    * @param characters The length of the message
    * @return If n is the length of the message:
    *         n < 5 => 0
    *         n >= 5 && n < 140 => n
    *         n >= 140 && n < 2000 => Gradually less then 140
    *         n > 2000 => 0
    */
  def activity(characters: Int): Int = {
    if(characters < 5) return 0
    if(characters < 140) return characters
    if(characters < 2000) return (140*(1-((characters-140)/2000.0))).toInt

    0
  }

  /**
    * Closes the competition.
    * @param kill The competition to close.
    */
  def killCompetition(kill: KillCompetition): Unit = {
    if (kill.competition != null) {
      if(guild.getSelfMember.hasPermission(Permission.MESSAGE_MANAGE)) {
        kill.competition.message.clearReactions.queue()
      }

      if (kill.competition == activeCompetition) {
        // It timed out before someone collected the crate.
        activeCompetition.message.editMessage("❌ The supply crate has rotten away.").queue()
        activeCompetition = null

        // Add back bribes if not used
        bribes ++= bribesForCompetition
        bribesForCompetition.clear()
      } else {
        // It timed out after the crate has been collected

        if(kill.competition == lastCompetition) {
          kill.competition.message.editMessage(lastCompetitionAwardMessage).queue()
          lastCompetitionAwardMessage = lastCompetitionAwardMessageStandard
          lastCompetition = null
          leftOverReceivedUsers.clear()
        }

        Main.bot ! Recalculate(guild, Collect())
      }

      if(shouldStop && bribes.isEmpty) {
        // If we should stop, do it now
        self ! PoisonPill
      }
    }
  }

  /**
    * Gets the penalty for a competition if someone clicks the reaction after the crate has been collected.
    * @param competition The competition to check for.
    * @return The penalty
    */
  def getLeftoversForCompetition(competition: Competition): Int = {
    val points = math.sqrt(competition.points.abs * 5).toInt

    if(competition.points < 0) {
      -points
    } else {
      points
    }
  }

  /**
    * Checks if a user is blocked.
    * @param member The member to check
    * @return True if the member is blocked, false otherwise.
    */
  def isBlocked(member: Member): Boolean = blockedMembers.contains(member.getUser.getIdLong)

  def interopabilityAcceptRedBotPoints(message: Message): Unit = {
    message.getRawContent match {
      case PATTERN_REDBOT_TRANSFER_MESSAGE(amount) ⇒
        var limit = 5
        message.getChannel.getIterableHistory.forEach{ messageCommand ⇒
          messageCommand.getRawContent match {
            case PATTERN_REDBOT_TRANSFER_COMMAND(prefix, amountCommand) ⇒
              if(amountCommand == amount && !messageCommand.getAuthor.isBot) {
                val actualAmount = try {
                  amount.toLong
                } catch {
                  case _: NumberFormatException ⇒ return
                }

                val member = messageCommand.getMember
                val points = (actualAmount - 10) / 6

                if(points >= Main.MAX_POINT_MODIFICATION) {
                  message.getChannel.sendMessage(Main.maxPointsModificationExceededMessage(member)).queue()
                  return
                }

                if(!isBlocked(member)) {
                  if(points > 0) {
                    modify(points, Some(PointTransfer()), member)
                    message.getChannel
                      .sendMessage(s"Thank you! I'll exchange that to $points points, ${member.getAsMention}!").queue()
                    return
                  } else {
                    message.getChannel.sendMessage(s"Aww, that would be nothing! Make sure to give at least 16 points.")
                      .queue()
                    return
                  }
                }
              }
            case _ ⇒
              limit -= 1
              if(limit <= 0) {
                message.getChannel.sendMessage(s"Unfortunately, I couldn't find your message with the command to figure out who sent me the points. Thats very unfortunate. If it's more then 999 points, contact the developer on his guild (link in ;help).").queue()
                return
              }
          }
        }
      case _ ⇒
    }
  }
}
