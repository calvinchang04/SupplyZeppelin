/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot

import akka.actor.SupervisorStrategy.{Resume, Stop}
import akka.actor.{Actor, ActorInitializationException, ActorKilledException, ActorRef, AllForOneStrategy, DeathPactException, Props, SupervisorStrategy}
import akka.event.Logging
import akka.routing.{DefaultResizer, SmallestMailboxPool}
import com.mongodb.client.result.UpdateResult
import de.garantiertnicht.supplyzepelin.Main
import de.garantiertnicht.supplyzepelin.bot.command.{CommandExecution, CommandManager}
import de.garantiertnicht.supplyzepelin.persistence.definitions.{DegradePoints, RemoveEmpty, RemoveScore}
import de.garantiertnicht.supplyzepelin.persistence.entities.rewards.Recurring
import net.dv8tion.jda.core.entities.{Guild, Member, Message, MessageReaction}
import org.mongodb.scala.Observer
import org.mongodb.scala.result.DeleteResult

import scala.collection.mutable
import scala.concurrent.duration._

case class GuildActorPoisoned(guild: Guild)
case class GuildCommand(message: Message, command: String)
case class GuildMessage(message: Message)
case class GuildReaction(reaction: MessageReaction, author: Member)
object StopSystem

class Bot extends Actor {
  val BIG_MASTER_SWITCH = false

  val map: mutable.Map[Long, ActorRef] = new mutable.HashMap[Long, ActorRef]
  val prefix = ";"
  val log = Logging(context.system, this)

  val commandResizer = DefaultResizer(lowerBound = 1, upperBound = 5)
  val commandDispatcher = context.actorOf(SmallestMailboxPool(1, Some(commandResizer)).props(Props(classOf[CommandManager], Seq
  (
    command.documentation.Help,
    command.fun.Bribe,
    command.management.Block,
    command.management.Unblock,
    command.management.Reward,
    command.points.Points,
    command.points.Scoreboard,
    command.fun.Transfer,
    command.fun.Redeem,
    command.system.Stop,
    command.system.DebugInfo,
    command.system.Stats,
    command.system.Canary,
    command.fun.Rewards
  ))), "command-dispatcher")

  val rewardResizer = DefaultResizer(lowerBound = 2, upperBound = 10)
  val rewardActor = context.actorOf(SmallestMailboxPool(1, Some(rewardResizer)).props(Props(classOf[RewardActor])))

  var shouldStop = false

  implicit val dispatcher = context.system.dispatcher

  override def preStart(): Unit = {
    context.system.scheduler.schedule(1 hour, 90 minutes, self, DegradePoints)
  }

  override def postStop(): Unit = {
    map.clear()
  }

  def getGuildActor(guild: Guild): ActorRef = {
    val option = map.get(guild.getIdLong)
    if(option.nonEmpty) {
      return option.get
    }

    if(!shouldStop && (!BIG_MASTER_SWITCH || guild.getIdLong == 204155906638872576L)) {
      try {
        val props = Props(classOf[GuildActor], guild)
        val actor = context.actorOf(props, s"${guild.getId}-${guild.getName.filter(char ⇒ char.isLetterOrDigit && char <= 127)}")
        map += ((guild.getIdLong, actor))

        log.debug(s"Guild ${guild.getName} (${guild.getId}) was loaded.")
      } catch {
        case e: Exception =>
          log.warning(s"Guild ${guild.getName} (${guild.getId}) failed to load:")
          e.printStackTrace()
      }
    }

    context.system.deadLetters
  }

  override def receive: Receive = {
    case message: GuildMessage =>
      if(!message.message.getAuthor.isBot) {
        val content = message.message.getRawContent
        val guildActor = getGuildActor(message.message.getGuild)
        if(content.startsWith(prefix)) {
          map.get(message.message.getGuild.getIdLong).foreach(_ ! GuildCommand(message.message, content.substring(prefix.length)))
        } else if(content.nonEmpty && content.charAt(0).isLetter) {
          map.get(message.message.getGuild.getIdLong).foreach(_ forward message)
        }
      } else {
        map.get(message.message.getGuild.getIdLong).foreach(_ ! GuildBotMessage(message.message))
      }
    case recalculate: Recalculate ⇒ rewardActor.forward(recalculate)
    case reaction: GuildReaction =>
      if(!reaction.author.getUser.isBot) {
        map.get(reaction.reaction.getGuild.getIdLong).foreach(_ forward reaction)
      }
    case command: CommandExecution => commandDispatcher ! command
    case remove: RemoveRewardFromMembers ⇒ rewardActor.forward(remove)
    case DegradePoints =>
      DegradePoints.mongoQuery.subscribe(new Observer[Unit] {
        override def onError(e: Throwable): Unit = {
          e.printStackTrace()
        }
        override def onNext(result: Unit): Unit = {}

        override def onComplete(): Unit = {
          RemoveScore.mongoQuery.subscribe(new Observer[UpdateResult] {
            override def onNext(result: UpdateResult): Unit = {}
            override def onError(e: Throwable): Unit = {
              e.printStackTrace()
            }
            override def onComplete(): Unit = {
              RemoveEmpty.mongoQuery.subscribe(new Observer[DeleteResult] {
                override def onError(e: Throwable): Unit = {
                  e.printStackTrace()
                }
                override def onComplete(): Unit = {}
                override def onNext(result: DeleteResult): Unit = {}
              })
            }
          })
        }
      })

      map.foreach {
        case (id, _) =>
          val guild = Main.jda.getGuildById(id)
          rewardActor ! Recalculate(guild, Recurring())
      }
    case isPoisoned: GuildActorPoisoned =>
      map.remove(isPoisoned.guild.getIdLong)
      log.debug(s"Guild ${isPoisoned.guild.getName} (${isPoisoned.guild.getId}) was unloaded.")
      if(shouldStop && map.isEmpty) {
        log.info("All guilds unloaded, waiting for queued actions")
        Thread.sleep(15000)
        Main.jda.shutdown()
        context.system.terminate().onComplete(sys.exit())
      }
    case StopSystem =>
      log.info("Shutting down…")
      shouldStop = true
      map.foreach(_._2 ! GuildActorShouldBePoisoned)
  }

  override def supervisorStrategy: SupervisorStrategy = AllForOneStrategy(maxNrOfRetries = 3, withinTimeRange = 10 minutes) {
    case _: ActorInitializationException => Stop
    case _: ActorKilledException         => Stop
    case _: DeathPactException           => Stop
    case e: Exception                    ⇒ {
      e.printStackTrace()
      Resume
    }
  }
}
