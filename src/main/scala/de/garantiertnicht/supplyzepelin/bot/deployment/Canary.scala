/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.deployment

import java.util.Date
import java.util.concurrent.TimeUnit

import de.garantiertnicht.supplyzepelin.Main
import net.dv8tion.jda.core.entities.{Guild, User}

import scala.concurrent.duration.Duration

abstract class Canary extends Deployment {
  val start: Long
  val endOnDevServer: Long
  val endAll: Long

  def start(isEligibleForDevGuildPrivileges: Boolean): Long = if(isEligibleForDevGuildPrivileges) {
    start
  } else {
    endOnDevServer
  }

  def end(isEligibleForDevGuildPrivileges: Boolean): Long = if(isEligibleForDevGuildPrivileges) {
    endOnDevServer
  } else {
    endAll
  }

  def isEligibleForDevGuildPrivileges(user: User): Boolean = {
    val ownerOnDevGuild = Canary.devGuild.getMember(user)
    ownerOnDevGuild != null && (ownerOnDevGuild.getJoinDate
      .toEpochSecond * 1000) < start
  }

  def isEligible(guild: Guild): Boolean = {
    if(guild == Canary.devGuild) return true

    val time = System.currentTimeMillis()

    if(time < start) return false

    val eligibleForDevGuildPrivileges: Boolean = isEligibleForDevGuildPrivileges(guild.getOwner.getUser)

    if(eligibleForDevGuildPrivileges && time > endOnDevServer) return true

    val realStart = start(eligibleForDevGuildPrivileges)
    val realEnd = end(eligibleForDevGuildPrivileges)

    if(time < realStart) return false
    if(time > realEnd) return true

    val progress: Short = (math.pow((time - realStart) / (realEnd.toDouble - realStart), 1.5) * Short.MaxValue).toShort
    val guildTicket = (guild.getIdLong + start) % Short.MaxValue

    guildTicket <= progress
  }
  override def eligibleAt(guild: Guild): Option[Duration] = {
    val eligibleForDevGuildPrivileges = isEligibleForDevGuildPrivileges(guild.getOwner.getUser)
    val realStart = start(eligibleForDevGuildPrivileges)
    val realEnd = end(eligibleForDevGuildPrivileges)

    val ticket = ((guild.getIdLong + start) % Short.MaxValue) / Short.MaxValue.toDouble
    val ticketAt = realStart + math.pow(ticket, 2.0/3.0) * (realEnd - realStart)

    Some(Duration(ticketAt.toLong - System.currentTimeMillis(), TimeUnit.MILLISECONDS))
  }
}

object Canary {
  // This being hardcoded is intentional
  // Someone just running the bot should use the same feedback line as others
  // If you extend on the bot, you are free to change this
  val devGuild: Guild = Main.jda.getGuildById(204155906638872576L)
}
