/*
 * Supply Zeppelin, a bot for Discord.
 * Copyright (C) 2017 garantiertnicht
 *
 * Supply Zeppelin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Supply Zeppelin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Supply Zeppelin. If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.supplyzepelin.bot.command.points

import akka.actor.ActorRef
import akka.util.Timeout
import de.garantiertnicht.supplyzepelin.bot.command.Command
import de.garantiertnicht.supplyzepelin.persistence.definitions.GetInfoForMember
import de.garantiertnicht.supplyzepelin.persistence.entities.ScoreboardEntry
import net.dv8tion.jda.core.entities.Message

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

object Points extends Command {
  override val name: String = "points"
  implicit val timeout = Timeout(1 minute)

  override def execute(commandAndArgs: Array[String], originalMessage: Message, guild: ActorRef)(implicit ex: ExecutionContext): Unit = {
    val member = if (originalMessage.getMentionedUsers.size() != 0) {
      val user = originalMessage.getMentionedUsers.get(0)
      val member = originalMessage.getGuild.getMember(user)

      if (member != null) {
        member
      } else {
        originalMessage.getMember
      }
    } else {
      originalMessage.getMember
    }

    new GetInfoForMember(member).mongoQuery.head().onSuccess {
      case info: ScoreboardEntry =>
        var senderInfo = if (member == originalMessage.getMember) {
          member.getAsMention
        } else {
          s"${originalMessage.getMember.getAsMention}: ${member.getEffectiveName}#${member.getUser.getDiscriminator}"
        }

        val score = info.score.total

        val message = if (score != 0) {
          s"$senderInfo has collected $score points so far!"
        } else {
          s"$senderInfo has not collected any points so far."
        }

        originalMessage.getChannel.sendMessage(message).queue()
      case null ⇒
        var senderInfo = if(member == originalMessage.getMember) {
          member.getAsMention
        } else {
          s"${originalMessage.getMember.getAsMention}: ${member.getEffectiveName}#${member.getUser.getDiscriminator}"
        }

        originalMessage.getChannel.sendMessage(s"$senderInfo has not collected any points so far.").queue()
    }
  }
}
